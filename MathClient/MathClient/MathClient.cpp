// MathClient.cpp : Client app for MathLibrary DLL.
#include "pch.h"

#include<iostream>
#include "MathLibrary.h"

using namespace std;

int main() {
	std::cout << "Addition is "; 
	std::cout << Add(2, 3);
	std::cout << "\nSubtraction is "; 
	std::cout << Sub(3, 2);
	std::cout << "\nMultiplication is "; 
	std::cout << Multiply(4, 5);
}
